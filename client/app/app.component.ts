import { Component } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import {FormBuilder,FormGroup} from '@angular/forms';

import "rxjs/add/operator/map";

@Component({
    selector: "my-app",
    templateUrl: `app/app.component.html`,
})
export class AppComponent {
    persons: Array<{}> = [];
    form: FormGroup;

    constructor(public http: Http, public fb:FormBuilder) {
        http
            .get("/api/persons")
            .map((response: Response) => response.json())
            .subscribe((data) => {
                this.persons = data;
                console.log(this.persons);
            });

        this.form = this.fb.group({
            first: [""],
            last:''
        });
    }

    whenIsAdded(data) {
        this.persons.push(data)
    }


}
