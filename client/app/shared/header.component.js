"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var forms_1 = require('@angular/forms');
var http_1 = require("@angular/http");
var HeaderComponent = (function () {
    function HeaderComponent(fb, http) {
        this.fb = fb;
        this.http = http;
        this.added = new core_1.EventEmitter();
        this.form = this.fb.group({
            name: '',
            address: '',
            phone: '',
            age: '',
            picture: ''
        });
    }
    HeaderComponent.prototype.insertPerson = function (person) {
        var _this = this;
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify(person);
        return this.http
            .post('http://localhost:3399/api/person/add', body, { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (response) {
            console.log(response[0]);
            _this.added.emit(response[0]);
        });
    };
    __decorate([
        core_1.Output("added"), 
        __metadata('design:type', core_1.EventEmitter)
    ], HeaderComponent.prototype, "added", void 0);
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-header',
            templateUrl: "app/shared/header.component.html"
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, http_1.Http])
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;
//# sourceMappingURL=header.component.js.map