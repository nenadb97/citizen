import {Component, Input, Output, EventEmitter} from "@angular/core";
import {FormBuilder,FormGroup, } from '@angular/forms';
import {Http, RequestOptions, Headers, Response, } from "@angular/http";

@Component({
    selector:'app-header',
    templateUrl:"app/shared/header.component.html"
})
export class HeaderComponent{
    form: FormGroup;
    @Output("added") added: EventEmitter<{}> = new EventEmitter<{}>();

    constructor(public fb: FormBuilder, public http: Http) {
        this.form = this.fb.group({
            name: '',
            address: '',
            phone: '',
            age: '',
            picture: ''
        });
    }

    insertPerson(person: {}) {

        let headers = new Headers({ 'Content-Type': 'application/json' })
        let options = new RequestOptions({ headers: headers });
        let body = JSON.stringify(person);

        return this.http
            .post('http://localhost:3399/api/person/add',body,{headers:headers})
            .map((res: Response) => res.json())
            .subscribe((response)=>{
                console.log(response[0]);
                this.added.emit(response[0]);
            });

        }
}
