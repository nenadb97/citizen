import * as express from "express";
import { MongoClient, Db, Collection } from "mongodb";

const bodyParser = require('body-parser');
const mongo = require('mongodb');

const conn: Promise<Db> = MongoClient.connect("mongodb://admin:admin@ds147267.mlab.com:47267/persondb");
const app = express();

let connection: Db;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(__dirname + "/../client"));
app.use(express.static(__dirname + "/../node_modules"));

app.get('/api/persons', (req, res) => {
    
    const persons = connection.collection('persons');
    
    persons.find({}).toArray((err, data) => {
        if (err) {
            return new Error(err.message);
        }

        return res.json(data);
    });
});

app.post('/api/person/add', (req, res) => {
    
    const persons: Collection = connection.collection('persons');

    persons
        .insert(req.body)
        .then((operation: { ops: Array<{}>}) => {
            return res.json(operation.ops);
        });    
});


conn.then((db: Db) => {
    
    connection = db;

    app.listen(3399, () => {
        console.log("Server started");
    });

});
