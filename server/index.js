"use strict";
var express = require("express");
var mongodb_1 = require("mongodb");
var bodyParser = require('body-parser');
var mongo = require('mongodb');
var conn = mongodb_1.MongoClient.connect("mongodb://admin:admin@ds147267.mlab.com:47267/persondb");
var app = express();
var connection;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/../client"));
app.use(express.static(__dirname + "/../node_modules"));
app.get('/api/persons', function (req, res) {
    var persons = connection.collection('persons');
    persons.find({}).toArray(function (err, data) {
        if (err) {
            return new Error(err.message);
        }
        return res.json(data);
    });
});
app.post('/api/person/add', function (req, res) {
    var persons = connection.collection('persons');
    persons
        .insert(req.body)
        .then(function (operation) {
        return res.json(operation.ops);
    });
});
conn.then(function (db) {
    connection = db;
    app.listen(3399, function () {
        console.log("Server started");
    });
});
//# sourceMappingURL=index.js.map